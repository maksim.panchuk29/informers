package main

import (
	"flag"
	"fmt"
	"path"

	databaseConfig "corelogic/internal/config/database"
	loggerConfig "corelogic/internal/config/logger"
	migrationConfig "corelogic/internal/config/migration"
	serverConfig "corelogic/internal/config/server"
)

var configPath string

func init() {
	defaultConfigPath := path.Join("config", "config.toml")
	flag.StringVar(&configPath, "config", defaultConfigPath, "Path to configuration file")
}

func main() {
	dbConfig, err := databaseConfig.New(configPath)
	if err != nil {
		fmt.Println(err)
		return
	}
	_ = dbConfig

	migrationConf, err := migrationConfig.New(configPath)
	if err != nil {
		fmt.Println(err)
		return
	}
	_ = migrationConf

	srvConfig, err := serverConfig.New(configPath)
	if err != nil {
		fmt.Println(err)
		return
	}
	_ = srvConfig

	logConfig, err := loggerConfig.New(configPath)
	if err != nil {
		fmt.Println(err)
		return
	}
	_ = logConfig

	fmt.Println("Hello!")
}
