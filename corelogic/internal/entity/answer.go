package entity

type Answer struct {
	ID            int
	Content       string
	DateParameter *DateParameter
}
