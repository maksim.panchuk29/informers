package entity

type Macros struct {
	ID            int
	Title         string
	DateParameter *DateParameter
}
