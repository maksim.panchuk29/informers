package entity

type Instruction struct {
	ID            int
	Content       string
	DateParameter *DateParameter
}
