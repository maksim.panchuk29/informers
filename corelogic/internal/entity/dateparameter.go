package entity

import "time"

type DateParameter struct {
	ID     int
	Open   time.Time
	Close  time.Time
	Delete time.Time
}
