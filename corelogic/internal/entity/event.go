package entity

type Event struct {
	ID            int
	Title         string
	Instruction   *Instruction
	Macros        *Macros
	DateParameter *DateParameter
}
