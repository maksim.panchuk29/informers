CREATE TABLE "instruction"(
                              "id" bigserial NOT NULL,
                              "content" VARCHAR(255) NOT NULL,
                              "date_id" BIGINT NOT NULL
);
ALTER TABLE
    "instruction" ADD PRIMARY KEY("id");
CREATE TABLE "answer"(
                         "id" bigserial NOT NULL,
                         "content" VARCHAR(255) NOT NULL,
                         "date_id" BIGINT NOT NULL
);
ALTER TABLE
    "answer" ADD PRIMARY KEY("id");
CREATE TABLE "event"(
                        "id" bigserial NOT NULL,
                        "title" VARCHAR(255) NOT NULL,
                        "instruction_id" BIGINT NOT NULL,
                        "macros_id" BIGINT NOT NULL,
                        "date_id" BIGINT NOT NULL
);
ALTER TABLE
    "event" ADD PRIMARY KEY("id");
CREATE TABLE "macros_answer"(
                                "id" bigserial NOT NULL,
                                "answer_id" BIGINT NOT NULL,
                                "macros_id" BIGINT NOT NULL
);
ALTER TABLE
    "macros_answer" ADD PRIMARY KEY("id");
CREATE TABLE "date_parameter"(
                                 "id" bigserial NOT NULL,
                                 "open_date" DATE NOT NULL,
                                 "close_date" DATE NOT NULL,
                                 "delete_date" DATE NOT NULL
);
ALTER TABLE
    "date_parameter" ADD PRIMARY KEY("id");
CREATE TABLE "event_rel"(
                            "id" bigserial NOT NULL,
                            "parent_id" BIGINT NOT NULL,
                            "child_id" BIGINT NOT NULL
);
ALTER TABLE
    "event_rel" ADD PRIMARY KEY("id");
CREATE TABLE "macros"(
                         "id" bigserial NOT NULL,
                         "title" VARCHAR(255) NOT NULL,
                         "date_id" BIGINT NOT NULL
);
ALTER TABLE
    "macros" ADD PRIMARY KEY("id");
ALTER TABLE
    "macros_answer" ADD CONSTRAINT "macros_answer_answer_id_foreign" FOREIGN KEY("answer_id") REFERENCES "answer"("id");
ALTER TABLE
    "event_rel" ADD CONSTRAINT "event_rel_child_id_foreign" FOREIGN KEY("child_id") REFERENCES "event"("id");
ALTER TABLE
    "event_rel" ADD CONSTRAINT "event_rel_parent_id_foreign" FOREIGN KEY("parent_id") REFERENCES "event"("id");
ALTER TABLE
    "answer" ADD CONSTRAINT "answer_date_id_foreign" FOREIGN KEY("date_id") REFERENCES "date_parameter"("id");
ALTER TABLE
    "macros_answer" ADD CONSTRAINT "macros_answer_macros_id_foreign" FOREIGN KEY("macros_id") REFERENCES "macros"("id");
ALTER TABLE
    "macros" ADD CONSTRAINT "macros_date_id_foreign" FOREIGN KEY("date_id") REFERENCES "date_parameter"("id");
ALTER TABLE
    "event" ADD CONSTRAINT "event_date_id_foreign" FOREIGN KEY("date_id") REFERENCES "date_parameter"("id");
ALTER TABLE
    "instruction" ADD CONSTRAINT "instruction_date_id_foreign" FOREIGN KEY("date_id") REFERENCES "date_parameter"("id");
ALTER TABLE
    "event" ADD CONSTRAINT "event_macros_id_foreign" FOREIGN KEY("macros_id") REFERENCES "macros"("id");
ALTER TABLE
    "event" ADD CONSTRAINT "event_instruction_id_foreign" FOREIGN KEY("instruction_id") REFERENCES "instruction"("id");