
DROP TABLE IF EXISTS node_rel CASCADE;
DROP TABLE IF EXISTS logic_answer CASCADE;
DROP TABLE IF EXISTS answer CASCADE;
DROP TABLE IF EXISTS date_parameter CASCADE;
DROP TABLE IF EXISTS node CASCADE;
DROP TABLE IF EXISTS logic CASCADE;

CREATE TABLE "node_rel"(
    "id" bigserial NOT NULL,
    "parent_id" BIGINT NOT NULL,
    "child_id" BIGINT NOT NULL
);

ALTER TABLE
    "node_rel" ADD PRIMARY KEY("id");
CREATE TABLE "answer"(
    "id" BIGINT NOT NULL,
    "content" BIGINT NOT NULL
);

ALTER TABLE
    "answer" ADD PRIMARY KEY("id");
CREATE TABLE "date_parameter"(
    "id" BIGINT NOT NULL,
    "open_date" BIGINT NOT NULL,
    "close_date" BIGINT NOT NULL,
    "delete_date" BIGINT NOT NULL
);

ALTER TABLE
    "date_parameter" ADD PRIMARY KEY("id");
CREATE TABLE "logic_answer"(
    "id" BIGINT NOT NULL,
    "logic_id" BIGINT NOT NULL,
    "answer_id" BIGINT NOT NULL
);

ALTER TABLE
    "logic_answer" ADD PRIMARY KEY("id");
CREATE TABLE "node"(
    "id" SERIAL NOT NULL,
    "logic_id" BIGINT NOT NULL,
    "locker_id" BIGINT NOT NULL,
    "date_id" BIGINT NOT NULL
);

ALTER TABLE
    "node" ADD PRIMARY KEY("id");
CREATE TABLE "logic"(
    "id" SERIAL NOT NULL,
    "type" SMALLINT NOT NULL,
    "title" VARCHAR(255) NOT NULL,
    "description" BIGINT NOT NULL,
    "locker_id" BIGINT NOT NULL,
    "date_id" BIGINT NOT NULL
);

ALTER TABLE
    "logic" ADD PRIMARY KEY("id");
ALTER TABLE
    "node_rel" ADD CONSTRAINT "node_rel_child_id_foreign" FOREIGN KEY("child_id") REFERENCES "node"("id");
ALTER TABLE
    "logic" ADD CONSTRAINT "logic_date_id_foreign" FOREIGN KEY("date_id") REFERENCES "date_parameter"("id");
ALTER TABLE
    "logic_answer" ADD CONSTRAINT "logic_answer_answer_id_foreign" FOREIGN KEY("answer_id") REFERENCES "answer"("id");
ALTER TABLE
    "node" ADD CONSTRAINT "node_logic_id_foreign" FOREIGN KEY("logic_id") REFERENCES "logic"("id");
ALTER TABLE
    "logic_answer" ADD CONSTRAINT "logic_answer_logic_id_foreign" FOREIGN KEY("logic_id") REFERENCES "logic"("id");
ALTER TABLE
    "node" ADD CONSTRAINT "node_date_id_foreign" FOREIGN KEY("date_id") REFERENCES "date_parameter"("id");
ALTER TABLE
    "node_rel" ADD CONSTRAINT "node_rel_parent_id_foreign" FOREIGN KEY("parent_id") REFERENCES "node"("id");